FROM node:13.10

LABEL maintainer="thomasdocker"

RUN mkdir /usr/src/app

WORKDIR /usr/src/app

COPY package.json ./

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "start"]