var express = require('express');
var app = express();
var Hospital = require('../models/hospital');
var middlewareAuthenticaion = require('../middlewares/authentication');

/**
 * @swagger
 * hospital/:
 *   get:
 *     description: Returns hospitals
 *     summary: Return hospital
 *     tags:
 *      - Hospitals
 *     produces:
 *      - application/json
 *     parameters:
 *      - in: query
 *        name: offset
 *        schema:
 *          type: integer
 *          maximum: 50
 *        examples:
 *          zero:
 *            value: 0
 *            summary: 10 
 *     responses:
 *       200:
 *         description: A hospital object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 hospitals:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                       nombre:
 *                         type: string
 *                       image:
 *                         type: string
 *                       user:
 *                         type: object
 *                         properties:
 *                           id:
 *                             type: string
 *                 total:
 *                   type: integer
 *               example:   # Sample object
 *                 status: true
 *                 description: Doctores
 *                 hospitals:
 *                   - id: 22
 *                     nombre: Hospital Norte
 *                     image: null
 *                     user:
 *                       - id: salkdasldnlj1231
 *                   - id: 23
 *                     nombre: Hospital Sur
 *                     image: null
 *                     user:
 *                       - id: salkdasldnlj1231
 *                   - id: 24
 *                     nombre: Hospital Este
 *                     image: null
 *                     user:
 *                       - id: salkdasldnlj1231
 *                 total: 3
 *                  
 */
app.get('/', (req, res, next) => {
    var offset = req.query.offset || 0;
    offset = Number(offset);

    Hospital.find({})
        .skip(offset)
        .limit(5)
        .populate('user', 'nombre email')
        .exec((err, Hospitals) => {
            if (err) {
                return res.status(500).json({
                    status: false,
                    description: 'Error loading hospitals.',
                    errors: err
                });
            }

            Hospital.estimatedDocumentCount({}, (err, quantity) => {
                res.status(200).json({
                    status: true,
                    description: '...',
                    hospital: Hospitals,
                    total: quantity
                }); // EVERYTHING OK
            });
        }); //GET
});

/**
 * @swagger
 * hospital/:id:
 *   get:
 *     description: Returns a hospital by id
 *     summary: get a hospital by id
 *     tags:
 *      - Hospitals
 *     produces:
 *      - application/json
 *     parameters:
 *      - in: query
 *        name: offset
 *        schema:
 *          type: integer
 *          maximum: 50
 *        examples:
 *          zero:
 *            value: 182731872hksadakjsdk
 *            summary: hospital id
 *     responses:
 *       200:
 *         description: A hospital object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 hospital:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     name:
 *                       type: string
 *                     image:
 *                       type: string
 *                     user:
 *                       type: object
 *                       properties:
 *                          id:
 *                           type: string                   
 *               example:
 *                 status: true
 *                 description: Hospital
 *                 hospital:
 *                   - id: 10
 *                     name: Dr. Jessica Smith
 *                     image: null
 *                     user:
 *                       - id: askdjaksd132132
 */
app.get('/:id', middlewareAuthenticaion.tokenVerification, (req, res, next) => {
    var idHospital = req.params.id;

    if (!idHospital) {
        return res.status(400).json({
            status: false,
            description: 'Error getting hospital',
            errors: err
        });
    }

    Hospital.findById(idHospital)
        .populate('user', 'nombre email')
        .exec((err, persistentHospital) => {
            if (err) {
                return res.status(500).json({
                    status: false,
                    description: 'Error getting hospital with id: ' + idHospital,
                    errors: err
                });
            }

            if (!persistentHospital) {
                return res.status(400).json({
                    status: false,
                    description: 'Error getting hospital with id: ' + idHospital,
                    errors: err
                });
            }

            res.status(200).json({
                status: true,
                description: '...',
                hospital: persistentHospital
            }); // EVERYTHING OK
        });
});

/**
 * @swagger
 * hospital/:
 *   post:
 *     tags:
 *      - Hospitals
 *     summary: Adds a new hospital
 *     description: create and return a hospital
 *     requestBody:
 *       content:
 *         application/json:
 *          schema:
 *            type: object
 *            properties:
 *               nombre:
 *                 type: string
 *               image:
 *                 type: string
 *               user:
 *                 type: string
 *            example:
 *              nombre: Hospital Saint Martin
 *              image: null
 *              user: asjljdnajsd1312312
 *     responses:
 *       201:
 *         description: A hospital object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 hospital:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     nombre:
 *                       type: string
 *                     image:
 *                       type: string
 *                     user:
 *                       type: object
 *                       properties:
 *                         id:
 *                           type: string                                  
 *               example:
 *                 status: true
 *                 description: Hospital
 *                 hospital:
 *                   - id: 7
 *                     nombre: Hospital Saint Martin
 *                     image: null
 *                     user: asjljdnajsd1312312   
 */
app.post('/', middlewareAuthenticaion.tokenVerification, (req, res, next) => {
    var body = req.body;

    var hospital = new Hospital({
        nombre: body.nombre,
        image: body.image,
        user: body.user
    });

    hospital.save((err, persistentHospital) => {
        if (err) {
            return res.status(400).json({
                status: false,
                description: 'Error creating hospital.',
                errors: err
            });
        }

        res.status(201).json({
            status: true,
            description: '...',
            hospital: persistentHospital,
            create_by: req.user
        }); // EVERYTHING OK
    });
});

/**
 * @swagger
 * hospital/:id:
 *   put:
 *     tags:
 *      - Hospitals
 *     summary: update a hospital by id
 *     description: update a hospital by id
 *     parameters:
 *      - in: query
 *        name: id
 *        schema:
 *          type: string
 *        examples:
 *          zero:
 *            value: 182731872hksadakjsdk
 *            summary: hospital id
 *     requestBody:
 *       content:
 *         application/json:
 *          schema:      # Request body contents
 *            type: object
 *            properties:
 *               nombre:
 *                 type: string
 *               image:
 *                 type: string
 *               user:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: string
 *            example:
 *              nombre: Hospital Este
 *              image: perfile.jpg
 *              user: asjdnasjkdn213
 *     responses:
 *       201:
 *         description: A hospital object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 hospital:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     nombre:
 *                       type: string
 *                     image:
 *                       type: string
 *                     user:
 *                       type: object
 *                       properties:
 *                         id:
 *                           type: string                   
 *               example:
 *                 status: true
 *                 description: Hospital
 *                 hospital:
 *                   - id: 1
 *                     nombre: Hospital Este
 *                     image: perfile.jpg
 *                     user:
 *                       - id: 12
 *                         nombre: Jessica Smith
 */
app.put('/:id', middlewareAuthenticaion.tokenVerification, (req, res, next) => {
    var id = req.params.id;
    var body = req.body;

    Hospital.findById(id, (err, persistentHospital) => {
        if (err) {
            return res.status(500).json({
                status: false,
                description: 'Error getting hospital',
                errors: err
            });
        }

        if (!persistentHospital) {
            return res.status(400).json({
                status: false,
                description: 'Hospital with id: ' + id + ' does not exist.',
                errors: { messages: '' }
            });
        }

        persistentHospital.nombre = body.nombre;
        persistentHospital.user = body.user;

        persistentHospital.save((err, updatedHospital) => {
            if (err) {
                return res.status(500).json({
                    status: false,
                    description: 'Error updating hospital',
                    errors: err
                });
            }

            res.status(201).json({
                status: true,
                description: '...',
                hospital: updatedHospital,
                updated_by: req.user
            }); // EVERYTHING OK
        });
    });
});

/**
 * @swagger
 * hospital/:id:
 *   delete:
 *     tags:
 *      - Hospitals
 *     summary: delete a hospital by id
 *     description: delete a hospital by id
 *     parameters:
 *      - in: query
 *        name: id
 *        schema:
 *          type: string
 *        examples:
 *          zero:
 *            value: 182731872hksadakjsdk
 *            summary: hospital  id
 *     responses:
 *       201:
 *         description: A hospital object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 hospital:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     nombre:
 *                       type: string
 *                     image:
 *                       type: string      
 *                     user:
 *                       type: object
 *                       properties:
 *                         id:
 *                           type: string      
 *                 delete_by:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     nombre:
 *                       type: string
 *                     image:
 *                       type: string      
 *                     email:
 *                       type: string      
 *                     password:
 *                       type: string  
 *                     rol:
 *                       type: string  
 *                     google:
 *                       type: boolean       
 *               example:
 *                 status: true
 *                 description: Users
 *                 hospital:
 *                   - id: 32
 *                     name: Hospital Santa Fé
 *                     image: perfile.jpg
 *                     user:
 *                        - id: 12
 *                          name: jessmith
 *                          image: null
 *                          password: xxxx
 *                 delete_by:
 *                   - id: 1
 *                     name: admin
 *                     image: null
 *                     password: xxxx
 */
app.delete(
    '/:id',
    middlewareAuthenticaion.tokenVerification,
    (req, res, next) => {
        var id = req.params.id;

        Hospital.findByIdAndRemove(id, (err, deletedHospital) => {
            if (err) {
                return res.status(500).json({
                    status: false,
                    description: 'Error deleting hospital',
                    errors: err
                });
            }

            if (!deletedHospital) {
                return res.status(500).json({
                    status: false,
                    description: 'Hospital: ' + id + ' does not exist.',
                    errors: { message: '...' }
                });
            }

            res.status(201).json({
                status: true,
                description: '...',
                hospital: deletedHospital,
                deleted_by: req.user
            }); // EVERYTHING OK
        });
    }
);

module.exports = app; //EXPORTA FUERA DE ESTE ARCHIVO