var express = require('express');
var app = express();
var Doctor = require('../models/doctor');
var middlewareAuthenticaion = require('../middlewares/authentication');

/**
 * @swagger
 * doctor/:
 *   get:
 *     description: Returns doctors
 *     summary: Return doctors
 *     tags:
 *      - Doctors
 *     produces:
 *      - application/json
 *     parameters:
 *      - in: query
 *        name: offset
 *        schema:
 *          type: integer
 *          maximum: 50
 *        examples:
 *          zero:
 *            value: 0
 *            summary: 10 
 *     responses:
 *       200:
 *         description: A doctor object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 users:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                       name:
 *                         type: string
 *                 total:
 *                   type: integer
 *               example:   # Sample object
 *                 status: true
 *                 description: Doctores
 *                 doctors:
 *                   - id: 10
 *                     name: Dr. Jessica Smith
 *                     image: null
 *                     hospital:
 *                       - id: 20
 *                         name: Hospital Fernandez
 *                         image: null
 *                     user:
 *                       - id: 12
 *                         name: jessmith
 *                         image: null
 *                         password: xxxx
 *                   - id: 10
 *                     name: Dr. Jessica Smith
 *                     image: null
 *                     hospital:
 *                       - id: 20
 *                         name: Hospital Fernandez
 *                         image: null
 *                     user:
 *                       - id: 12
 *                         name: jessmith
 *                         image: null
 *                         password: xxxx
 *                 total: 2
 *                  
 */
app.get('/', (req, res, next) => {
    var offset = req.query.offset || 0;
    offset = Number(offset);

    Doctor.find({})
        .skip(offset)
        .limit(5)
        .populate('user', 'nombre email')
        .populate('hospital')
        .exec((err, doctors) => {
            if (err) {
                return res.status(500).json({
                    status: false,
                    description: 'Error loading doctors.',
                    errors: err
                });
            }

            Doctor.estimatedDocumentCount({}, (err, quantity) => {
                res.status(200).json({
                    status: true,
                    description: '...',
                    doctors: doctors,
                    total: quantity
                }); // EVERYTHING OK
            });
        }); //GET
});

/**
 * @swagger
 * doctor/:id:
 *   get:
 *     description: Returns a doctor by id
 *     summary: get a doctor by id
 *     tags:
 *      - Doctors
 *     produces:
 *      - application/json
 *     parameters:
 *      - in: query
 *        name: offset
 *        schema:
 *          type: integer
 *          maximum: 50
 *        examples:
 *          zero:
 *            value: 182731872hksadakjsdk
 *            summary: doctor  id
 *     responses:
 *       200:
 *         description: A doctor object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 doctor:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     name:
 *                       type: string
 *                     image:
 *                       type: string
 *                     hospital:
 *                       type: object
 *                       properties:
 *                          id:
 *                           type: integer
 *                          user:
 *                            type: object
 *                            properties:
 *                              id:
 *                                type: integer                     
 *               example:   # Sample object
 *                 status: true
 *                 description: Doctor
 *                 doctor:
 *                   - id: 10
 *                     name: Dr. Jessica Smith
 *                     image: null
 *                     hospital:
 *                       - id: 20
 *                         name: Hospital Fernandez
 *                         image: null
 *                     user:
 *                        - id: 12
 *                          name: jessmith
 *                          image: null
 *                          password: xxxx
 */
app.get('/:id', (req, res, next) => {
    var id = req.params.id;

    if (!id || id === '') {
        return res.status(400).json({
            status: false,
            description: 'Id should not be empty.',
            errors: { messages: '' }
        });
    }

    Doctor.findById(id)
        .populate('user', 'nombre email')
        .populate('hospital')
        .exec((err, persistentDoctor) => {
            if (err) {
                return res.status(500).json({
                    status: false,
                    description: 'Error loading doctors.',
                    errors: err
                });
            }

            if (!persistentDoctor) {
                return res.status(400).json({
                    status: false,
                    description: 'Doctor with id: ' + id + ' does not exist.',
                    errors: { messages: '' }
                });
            }

            res.status(200).json({
                status: true,
                description: '...',
                doctor: persistentDoctor
            }); // EVERYTHING OK
        }); //GET
});

/**
 * @swagger
 * doctor/:
 *   post:
 *     tags:
 *      - Doctors
 *     summary: Adds a new doctor
 *     description: create and return a doctor
 *     requestBody:
 *       content:
 *         application/json:
 *          schema:      # Request body contents
 *            type: object
 *            properties:
 *               nombre:
 *                 type: string
 *               image:
 *                 type: string
 *               user:
 *                 type: integer
 *               hospital:
 *                 type: integer
 *            example:   # Sample object
 *              nombre: Jessica Smith
 *              image: null
 *              user: ahsbdajsbdashd123
 *              hospital: asdasdasdass
 *     responses:
 *       201:
 *         description: A doctor object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 doctor:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     name:
 *                       type: string
 *                     image:
 *                       type: string
 *                     hospital:
 *                       type: object
 *                       properties:
 *                          id:
 *                           type: integer
 *                     user:
 *                       type: object
 *                       properties:
 *                         id:
 *                          type: integer                     
 *               example:   # Sample object
 *                 status: true
 *                 description: Doctor
 *                 doctor:
 *                   - id: 10
 *                     name: Dr. Jessica Smith
 *                     image: null
 *                     hospital:
 *                       - id: 20
 *                         name: Hospital Fernandez
 *                         image: null
 *                     user:
 *                        - id: 12
 *                          name: jessmith
 *                          image: null
 *                          password: xxxx
 */
app.post('/', middlewareAuthenticaion.tokenVerification, (req, res, next) => {
    var body = req.body;

    var doctor = new Doctor({
        nombre: body.nombre,
        image: body.image,
        user: body.user,
        hospital: body.hospital
    });

    doctor.save((err, persistentDoctor) => {
        if (err) {
            return res.status(400).json({
                status: false,
                description: 'Error creating doctor.',
                errors: err
            });
        }

        res.status(201).json({
            status: true,
            description: '...',
            doctor: persistentDoctor,
            create_by: req.user
        }); // EVERYTHING OK
    });
});

/**
 * @swagger
 * doctor/:id:
 *   put:
 *     tags:
 *      - Doctors
 *     summary: updte a doctor by id
 *     description: updte a doctor by id
 *     parameters:
 *      - in: query
 *        name: id
 *        schema:
 *          type: string
 *        examples:
 *          zero:
 *            value: 182731872hksadakjsdk
 *            summary: doctor  id
 *     requestBody:
 *       content:
 *         application/json:
 *          schema:      # Request body contents
 *            type: object
 *            properties:
 *               nombre:
 *                 type: string
 *               image:
 *                 type: string
 *               user:
 *                 type: integer
 *               hospital:
 *                 type: integer
 *            example:   # Sample object
 *              nombre: Jessica Lucciana Smith
 *              image: perfile.jpg
 *              user: ahsbdajsbdashd123
 *              hospital: asdasdasdass
 *     responses:
 *       201:
 *         description: A doctor object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 doctor:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     name:
 *                       type: string
 *                     image:
 *                       type: string
 *                     hospital:
 *                       type: object
 *                       properties:
 *                          id:
 *                           type: integer
 *                     user:
 *                       type: object
 *                       properties:
 *                         id:
 *                          type: integer                     
 *               example:   # Sample object
 *                 status: true
 *                 description: Doctor
 *                 doctor:
 *                   - id: 10
 *                     name: Dr. Jessica Lucciana Smith
 *                     image: perfile.jpg
 *                     hospital:
 *                       - id: 20
 *                         name: Hospital Fernandez
 *                         image: null
 *                     user:
 *                        - id: 12
 *                          name: jessmith
 *                          image: null
 *                          password: xxxx
 */
app.put('/:id', middlewareAuthenticaion.tokenVerification, (req, res, next) => {
    var id = req.params.id;
    var body = req.body;

    Doctor.findById(id, (err, persistentDoctor) => {
        if (err) {
            return res.status(500).json({
                status: false,
                description: 'Error getting doctor',
                errors: err
            });
        }

        if (!persistentDoctor) {
            return res.status(400).json({
                status: false,
                description: 'Doctor with id: ' + id + ' does not exist.',
                errors: { messages: '' }
            });
        }

        persistentDoctor.nombre = body.nombre;
        persistentDoctor.user = body.user;
        persistentDoctor.hospital = body.hospital;

        persistentDoctor.save((err, updatedDoctor) => {
            if (err) {
                return res.status(500).json({
                    status: false,
                    description: 'Error updating doctor',
                    errors: err
                });
            }

            res.status(201).json({
                status: true,
                description: '...',
                doctor: updatedDoctor,
                updated_by: req.user
            }); // EVERYTHING OK
        });
    });
});

/**
 * @swagger
 * doctor/:id:
 *   delete:
 *     tags:
 *      - Doctors
 *     summary: delete a doctor by id
 *     description: delete a doctor by id
 *     parameters:
 *      - in: query
 *        name: id
 *        schema:
 *          type: string
 *        examples:
 *          zero:
 *            value: 182731872hksadakjsdk
 *            summary: doctor  id
 *     responses:
 *       201:
 *         description: A doctor object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 doctor:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     name:
 *                       type: string
 *                     image:
 *                       type: string
 *                     hospital:
 *                       type: object
 *                       properties:
 *                          id:
 *                           type: integer
 *                     user:
 *                       type: object
 *                       properties:
 *                         id:
 *                          type: integer
 *                 delete_by:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     nombre:
 *                       type: string
 *                     image:
 *                       type: string      
 *                     email:
 *                       type: string      
 *                     password:
 *                       type: string  
 *                     rol:
 *                       type: string  
 *                     google:
 *                       type: boolean       
 *               example:   # Sample object
 *                 status: true
 *                 description: Doctor
 *                 doctor:
 *                   - id: 10
 *                     name: Dr. Jessica Lucciana Smith
 *                     image: perfile.jpg
 *                     hospital:
 *                       - id: 20
 *                         name: Hospital Fernandez
 *                         image: null
 *                     user:
 *                        - id: 12
 *                          name: jessmith
 *                          image: null
 *                          password: xxxx
 *                 delete_by:
 *                   - id: 1
 *                     name: admin
 *                     image: null
 *                     password: xxxx
 */
app.delete(
    '/:id',
    middlewareAuthenticaion.tokenVerification,
    (req, res, next) => {
        var id = req.params.id;

        Doctor.findByIdAndRemove(id, (err, deletedDoctor) => {
            if (err) {
                return res.status(500).json({
                    status: false,
                    description: 'Error deleting doctor',
                    errors: err
                });
            }

            if (!deletedDoctor) {
                return res.status(500).json({
                    status: false,
                    description: 'Doctor: ' + id + ' does not exist.',
                    errors: { message: '...' }
                });
            }

            res.status(201).json({
                status: true,
                description: '...',
                doctor: deletedDoctor,
                deleted_by: req.user
            }); // EVERYTHING OK
        });
    }
);

module.exports = app; //EXPORTA FUERA DE ESTE ARCHIVO