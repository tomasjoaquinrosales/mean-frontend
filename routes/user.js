var express = require('express');
var app = express();
var bcrypt = require('bcrypt');
var User = require('../models/user');
var middlewareAuthentication = require('../middlewares/authentication');

/**
 * @swagger
 * user/:
 *   get:
 *     description: Returns users
 *     summary: Return user
 *     tags:
 *      - Users
 *     produces:
 *      - application/json
 *     parameters:
 *      - in: query
 *        name: offset
 *        schema:
 *          type: integer
 *          maximum: 50
 *        examples:
 *          zero:
 *            value: 0
 *            summary: 10 
 *     responses:
 *       200:
 *         description: A user object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 users:
 *                   type: array
 *                   items:
 *                     type: object
 *                     properties:
 *                       id:
 *                         type: integer
 *                       nombre:
 *                         type: string
 *                       email:
 *                         type: string
 *                       password:
 *                         type: string
 *                       image:
 *                         type: string
 *                       rol:
 *                         type: string
 *                       google:
 *                         type: boolean
 *                 total:
 *                   type: integer
 *               example:   # Sample object
 *                 status: true
 *                 description: Doctores
 *                 doctors:
 *                   - id: 11
 *                     nombre: jessismith
 *                     email: jessi@smith.com.ar
 *                     password: secret
 *                     image: null
 *                     rol: ___ROL
 *                     google: false
 *                   - id: 12
 *                     nombre: jessismith2
 *                     email: jessi2@smith.com.ar
 *                     password: secret
 *                     image: null
 *                     rol: ___ROL
 *                     google: false
 *                   - id: 13
 *                     nombre: jessismith3
 *                     email: jessi3@smith.com.ar
 *                     password: secret
 *                     image: null
 *                     rol: ___ROL
 *                     google: false
 *                 total: 3
 *                  
 */
app.get('/', (req, res, next) => {
    var offset = req.query.offset || 0;
    offset = Number(offset);

    User.find({}, 'nombre email image rol google')
        .skip(offset)
        .limit(5)
        .exec((err, Users) => {
            if (err) {
                return res.status(500).json({
                    status: false,
                    description: 'Error loading users.',
                    errors: err
                });
            }

            User.estimatedDocumentCount({}, (err, quantity) => {
                res.status(200).json({
                    status: true,
                    description: '...',
                    users: Users,
                    total: quantity
                }); // EVERYTHING OK
            });
        }); //GET
});

/**
 * MIDDLEWARE SE PASO A OTRO ARCHIVO
 */
/*app.use('/', (req, res, next) => {

    var token = req.query.token;

    jwt.verify(token, SECRET_KEY, (err, decode) => {
        if (err) {
            return res.status(401).json({
                status: false,
                description: 'Incorrect Token',
                errors: err
            });
        }
        next(); //CONTINUA CON LA EJECUCION NORMAL DE LOS OTROS METODOS
    });
});*/

/**
 * @swagger
 * user/:
 *   post:
 *     tags:
 *      - Users
 *     summary: Adds a new user
 *     description: create and return a user
 *     requestBody:
 *       content:
 *         application/json:
 *          schema:
 *            type: object
 *            properties:
 *               nombre:
 *                 type: string
 *               image:
 *                 type: string
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *            example:   # Sample object
 *              nombre: Jessica Smith
 *              image: null
 *              email: jessi@smith.com
 *              password: secretPASSword
 *     responses:
 *       201:
 *         description: A user object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 user:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     nombre:
 *                       type: string
 *                     email:
 *                       type: string
 *                     password:
 *                       type: string
 *                     image:
 *                       type: string
 *                     rol:
 *                       type: string
 *                     google:
 *                       type: boolean                                  
 *               example:
 *                 status: true
 *                 description: User
 *                 user:
 *                   - id: 10
 *                     nombre: Jessica Smith
 *                     image: null
 *                     email: jessi@smith.com
 *                     password: secretPASSword     
 */
app.post('/', (req, res) => {
    var body = req.body; // BODY-PARSER PACKAGE CAPTURA Y PARSEA LOS PARAMETROS ENVIADOS POR JSON

    const saltRounds = 10;
    const salt = bcrypt.genSaltSync(saltRounds);

    var user = new User({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, salt),
        image: body.image,
        rol: body.rol
    });

    user.save((err, persistentUser) => {
        if (err) {
            return res.status(400).json({
                status: false,
                description: 'Error creating users.',
                errors: err
            });
        }

        res.status(201).json({
            status: true,
            description: '...',
            user: persistentUser,
            create_by: req.user
        }); // EVERYTHING OK
    });
});

/**
 * @swagger
 * user/:id:
 *   put:
 *     tags:
 *      - Users
 *     summary: update a user by id
 *     description: update a user by id
 *     parameters:
 *      - in: query
 *        name: id
 *        schema:
 *          type: string
 *        examples:
 *          zero:
 *            value: 182731872hksadakjsdk
 *            summary: user id
 *     requestBody:
 *       content:
 *         application/json:
 *          schema:      # Request body contents
 *            type: object
 *            properties:
 *               nombre:
 *                 type: string
 *               image:
 *                 type: file
 *               rol:
 *                 type: string
 *               password:
 *                 type: string
 *            example:   # Sample object
 *              nombre: Jessica Lucciana Smith
 *              image: perfile.jpg
 *              rol: ___ROL
 *              password: newSecret
 *     responses:
 *       201:
 *         description: A user object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 user:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     nombre:
 *                       type: string
 *                     image:
 *                       type: string      
 *                     email:
 *                       type: string      
 *                     password:
 *                       type: string  
 *                     rol:
 *                       type: string  
 *                     google:
 *                       type: boolean                   
 *               example:   # Sample object
 *                 status: true
 *                 description: Doctor
 *                 doctor:
 *                   - id: 10
 *                     nombre: Jessica Lucciana Smith
 *                     image: perfile.jpg
 *                     rol: ___ROL
 *                     password: newSecretHash
 */
app.put(
    '/:id', [
        middlewareAuthentication.tokenVerification,
        middlewareAuthentication.profileVerification
    ],
    (req, res) => {
        var id = req.params.id;
        var body = req.body; // PARAMETROS NUEVOS

        User.findById(id, (err, user) => {
            if (err) {
                return res.status(500).json({
                    status: false,
                    description: 'Error getting user',
                    errors: err
                });
            }

            if (!user) {
                return res.status(400).json({
                    status: false,
                    description: 'User dont exist: ' + id,
                    errors: { message: '...' }
                });
            }

            user.nombre = body.nombre;
            user.email = body.email;
            user.rol = body.rol;

            user.save((err, persistentUser) => {
                if (err) {
                    return res.status(500).json({
                        status: false,
                        description: 'Error updating user',
                        errors: err
                    });
                }

                persistentUser.password = '*******';

                res.status(200).json({
                    status: true,
                    description: '...',
                    user: persistentUser,
                    update_by: req.user
                }); // EVERYTHING OK
            });
        });
    }
);

/**
 * @swagger
 * user/:id:
 *   delete:
 *     tags:
 *      - Users
 *     summary: delete a user by id
 *     description: delete a user by id
 *     parameters:
 *      - in: query
 *        name: id
 *        schema:
 *          type: string
 *        examples:
 *          zero:
 *            value: 182731872hksadakjsdk
 *            summary: doctor  id
 *     responses:
 *       201:
 *         description: A user object. 
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 status: 
 *                   type: boolean
 *                 description:
 *                   type: string
 *                 user:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     nombre:
 *                       type: string
 *                     image:
 *                       type: string      
 *                     email:
 *                       type: string      
 *                     password:
 *                       type: string  
 *                     rol:
 *                       type: string  
 *                     google:
 *                       type: boolean 
 *                 delete_by:
 *                   type: object
 *                   properties:
 *                     id:
 *                       type: integer
 *                     nombre:
 *                       type: string
 *                     image:
 *                       type: string      
 *                     email:
 *                       type: string      
 *                     password:
 *                       type: string  
 *                     rol:
 *                       type: string  
 *                     google:
 *                       type: boolean       
 *               example:   # Sample object
 *                 status: true
 *                 description: Users
 *                 user:
 *                   - id: 10
 *                     name: Dr. Jessica Lucciana Smith
 *                     image: perfile.jpg
 *                     hospital:
 *                       - id: 20
 *                         name: Hospital Fernandez
 *                         image: null
 *                     user:
 *                        - id: 12
 *                          name: jessmith
 *                          image: null
 *                          password: xxxx
 *                 delete_by:
 *                   - id: 1
 *                     name: admin
 *                     image: null
 *                     password: xxxx
 */
app.delete(
    '/:id', [
        middlewareAuthentication.tokenVerification,
        middlewareAuthentication.adminVerification
    ],
    (req, res) => {
        var id = req.params.id;

        User.findByIdAndRemove(id, (err, deletedUser) => {
            if (err) {
                return res.status(500).json({
                    status: false,
                    description: 'Error deleting user',
                    errors: err
                });
            }
            if (!deletedUser) {
                return res.status(500).json({
                    status: false,
                    description: 'User: ' + id + ' does not exist.',
                    errors: { message: '...' }
                });
            }

            res.status(200).json({
                status: true,
                description: '...',
                user: deletedUser,
                delete_by: req.user
            }); // EVERYTHING OK
        });
    }
);

module.exports = app; //EXPORTA FUERA DE ESTE ARCHIVO