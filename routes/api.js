var express = require('express');
var app = express();
const swaggerJSDoc = require('swagger-jsdoc');

//SWAGGER
// Swagger set up
var swaggerDefinition = {
    openapi: "3.0.0",
    info: {
        title: "Time to document that Express API you built",
        version: "1.0.0",
        description: "A test project to understand how easy it is to document and Express API",
        license: {
            name: "MIT",
            url: "https://choosealicense.com/licenses/mit/"
        },
        contact: {
            name: "Swagger",
            url: "https://swagger.io",
            email: "Info@SmartBear.com"
        }
    },
    basePath: '/',
    servers: [{
        url: "http://localhost:3000/api-docs"
    }],
};

var options = {
    swaggerDefinition: swaggerDefinition,
    apis: ['routes/*.js', './models/*.js'],
};

var swaggerSpec = swaggerJSDoc(options);

module.exports.swaggerSpec = swaggerSpec;