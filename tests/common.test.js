// Import the dependencies for testing
const expect = require('chai').expect;
const should = require('chai').should();
const chai = require('chai');
const chaiHttp = require('chai-http');
const request = require("supertest");
const bcrypt = require('bcrypt');
var env = require('dotenv').config({ path: './.env' });

var User = require("../models/user");
var Doctor = require("../models/doctor");
var Hospital = require("../models/hospital");

var upload_local = process.env.TEST_UPLOAD_LOCAL || '/builds/tomasjoaquinrosales/mean-frontend/tests/upload/';

const url = 'http://localhost:3000';

chai.use(chaiHttp);

const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);

const defaultAdmin = {
    nombre: "expressadmin",
    email: "expressadmin@gmail.com",
    password: bcrypt.hashSync("adMIN123$", salt),
    image: null,
    rol: "ADMIN_ROL"
};

var defaultHospital = {
    nombre: "Hospital Dr. Fake",
    image: null,
    user: null
};

var defaultDoctor = {
    nombre: "Dr. Foreman",
    image: null,
    user: null,
    hospital: null
}


const createAdmin = async() => {
    const UserModel = new User(defaultAdmin);
    await UserModel.save();
}

const createDefaultHospital = async() => {
    var user = await getDefaultUser();
    defaultHospital.user = user._id;
    const HospitalModel = new Hospital(defaultHospital);
    await HospitalModel.save();
}

const createDefaultDoctor = async() => {
    var user = await getDefaultUser();
    var hospital = await getDefaultHospital();
    defaultDoctor.user = user._id;
    defaultDoctor.hospital = hospital._id;
    const DoctorModel = new Doctor(defaultDoctor);
    await DoctorModel.save();
}

const getDefaultUser = async() => {
    var users = await User.find({ "email": defaultAdmin.email });
    if (users.length === 0) {
        await createAdmin();
        return getDefaultUser();
    } else {
        return users[0];
    }
};

const getDefaultHospital = async() => {
    var hospital = await Hospital.find({ "nombre": defaultHospital.nombre });
    if (hospital.length === 0) {
        await createDefaultHospital();
        return getDefaultHospital();
    } else {
        return hospital[0];
    }
};

const getDefaultDoctor = async() => {
    var doctor = await Doctor.find({ "nombre": defaultDoctor.nombre });
    if (doctor.length === 0) {
        await createDefaultDoctor();
        return getDefaultDoctor();
    } else {
        return doctor[0];
    }
};

const loginWithDefaultUser = async() => {
    var user = await getDefaultUser();
    return request(url)
        .post('/login')
        .send({
            email: "expressadmin@gmail.com",
            password: "adMIN123$"
        })
        .expect(200);
};

const cleanAllUsers = async() => {
    var user = await getDefaultUser();
    await User.deleteMany({}, function(err, res) {
        if (err) {
            return err;
        }
        return res;
    });
};

const cleanAllHospitals = async() => {
    var hospital = await getDefaultHospital();
    await Hospital.deleteMany({}, function(err, res) {
        if (err) {
            return err;
        }
        return res;
    })
}

const cleanAllDoctors = async() => {
    var doctor = await getDefaultDoctor();
    await Doctor.deleteMany({}, function(err, res) {
        if (err) {
            return err;
        }
        return res;
    })
}

module.exports.loginWithDefaultUser = loginWithDefaultUser;
module.exports.getDefaultHospital = getDefaultHospital;
module.exports.getDefaultDoctor = getDefaultDoctor;
module.exports.cleanAllUsers = cleanAllUsers;
module.exports.cleanAllHospitals = cleanAllHospitals;
module.exports.cleanAllDoctors = cleanAllDoctors;
module.exports.upload_local = upload_local;