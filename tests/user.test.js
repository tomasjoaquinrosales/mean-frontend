const common = require("./common.test");

const expect = require('chai').expect;
const should = require('chai').should();
const chai = require('chai');
const chaiHttp = require('chai-http');
const request = require("supertest");

var User = require("../models/user");
const url = 'http://localhost:3000';

const app = require("../app");

chai.use(chaiHttp);

var bcrypt = require('bcrypt');

describe('User', () => {

    var token = null;
    var persistentUser = null;

    //get token
    before(async() => {
        var resToken = await common.loginWithDefaultUser();
        token = resToken.body.token;
        //console.log(token);
    });

    // delete user default
    after(async() => {
        await common.cleanAllUsers();
    });

    it('should get token', (done) => {
        common.loginWithDefaultUser().then(res => {
            done();
        });
    });

    it('should create a invalid user', (done) => {
        chai.request(url)
            .post('/user')
            .send({
                nombre: "",
                email: "",
                password: "",
                rol: "",
            }).end(function(err, res) {
                if (err) return done(err);
                expect(res).to.have.status(400);
                done();
            });
    });

    it('should create a valid user', (done) => {
        chai.request(url)
            .post('/user')
            .send({
                nombre: "testexpress",
                email: "testexpress@gmail.com",
                password: "adMIN123$",
                rol: "USER_ROL"
            })
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body).to.be.an('object');
                expect(bcrypt.compareSync('adMIN123$', res.body.user.password)).to.be.true
                expect(res.body.user).to.have.deep.property('email', 'testexpress@gmail.com');
                expect(res).to.have.status(201);
                persistentUser = res.body.user;
                done();
            });
    });

    it('should get all user', (done) => {
        chai.request(url)
            .get('/user')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.total === res.body.users.length).to.be.true
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should update user properties (nombre, email)', (done) => {
        persistentUser.nombre = "test";
        persistentUser.email = "test@test.com.ar";

        chai.request(url)
            .put('/user/' + persistentUser._id + '?token=' + token)
            .send({
                nombre: persistentUser.nombre,
                email: persistentUser.email,
                rol: persistentUser.rol
            })
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body).to.be.an('object');
                expect(res.body.user).to.have.deep.property('email', persistentUser.email);
                expect(res.body.user).to.have.deep.property('nombre', persistentUser.nombre);
                expect(res.body.user).to.have.deep.property('rol', persistentUser.rol);
                expect(res).to.have.status(200);
                done();
            });
    });


    it('should change user password', (done) => {
        persistentUser.password = "1234";

        chai.request(url)
            .put('/user/' + persistentUser._id + '?token=' + token)
            .send({
                nombre: persistentUser.nombre,
                email: persistentUser.email,
                password: persistentUser.password,
                rol: persistentUser.rol
            })
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body).to.be.an('object');
                expect(bcrypt.compareSync(persistentUser.password, res.body.user.password)).to.be.false
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should delete user', (done) => {
        chai.request(url)
            .delete('/user/' + persistentUser._id + '?token=' + token)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.user).to.have.deep.property('_id', persistentUser._id);
                expect(res.body.user._id == persistentUser._id).to.be.true;
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should not delete user that not exist', (done) => {
        chai.request(url)
            .delete('/user/10000?token=' + token)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.description).to.have.deep.equal('Error deleting user');
                expect(res.body.status).to.be.false;
                expect(res).to.have.status(500);
                done();
            });
    });

});