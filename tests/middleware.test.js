const common = require("./common.test");

const expect = require('chai').expect;
const should = require('chai').should();
const chai = require('chai');
const chaiHttp = require('chai-http');
const request = require("supertest");

var User = require("../models/user");
const url = 'http://localhost:3000';

const app = require("../app");

chai.use(chaiHttp);

var bcrypt = require('bcrypt');

describe('Finder', () => {

    var token = null;
    var tokenStaffUser = null;
    var persistentUser = null;
    var staffUser = null;
    var persistentHospital = null;

    //get token
    before(async() => {
        var response = await common.loginWithDefaultUser();
        token = response.body.token;
        //console.log(token);S
        persistentUser = response.body.user;

        persistentHospital = await common.getDefaultHospital();

        request(url)
            .post('/user')
            .send({
                nombre: "testexpress",
                email: "testexpress@gmail.com",
                password: "adMIN123$",
                rol: "USER_ROL"
            })
            .end(function(err, res) {
                if (err) return done(err);
                staffUser = res.body.user;
            });
    });

    // delete user default
    after(async() => {
        await common.cleanAllUsers();
        await common.cleanAllHospitals();
    });

    it('should get token', (done) => {
        common.loginWithDefaultUser().then(res => {
            done();
        });
    });

    it('should faild by badly token', (done) => {
        chai.request(url)
            .post('/hospital?token=' + token + 'u')
            .send({
                nombre: "Hospital Robinson Jr. H",
                user: persistentUser._id
            })
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body).to.deep.equal({ status: false, description: 'Incorrect Token', errors: { name: 'JsonWebTokenError', message: 'invalid signature' } });
                expect(res).to.have.status(401);
                done();
            });
    });

    it('should login with staff user', (done) => {
        chai.request(url)
            .post('/login')
            .send({
                email: "testexpress@gmail.com",
                password: "adMIN123$"
            })
            .end(function(err, res) {
                if (err) return done(err);
                tokenStaffUser = res.body.token;
                done();
            });
    })

    it('should faild by badly admin rol', (done) => {
        chai.request(url)
            .delete('/user/' + persistentUser._id + '?token=' + tokenStaffUser)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body).to.deep.equal({ status: false, description: 'Incorrect Token', errors: { mesage: 'inorrect token admin.' } });
                expect(res).to.have.status(401);
                done();
            });
    });



});