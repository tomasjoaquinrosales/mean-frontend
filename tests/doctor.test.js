const common = require("./common.test");

var bcrypt = require('bcrypt');

// Import the dependencies for testing
const expect = require('chai').expect;
const should = require('chai').should();
const chai = require('chai');
const chaiHttp = require('chai-http');
const request = require("supertest");

var User = require("../models/user");
var Doctor = require("../models/doctor");
var Hospital = require("../models/hospital");

const url = 'http://localhost:3000';

const app = require("../app");

chai.use(chaiHttp);

const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);


describe('Doctor', () => {

    var token = null;
    var persistentUser = null;
    var persistentHospital = null;
    var doctor = null;

    //get token
    before(async() => {
        var response = await common.loginWithDefaultUser();
        token = response.body.token;
        //console.log(token);
        persistentUser = response.body.user;
        persistentHospital = await common.getDefaultHospital();
    });

    // delete user and hospital default
    after(async() => {
        await common.cleanAllUsers();
        await common.cleanAllHospitals();
    });

    it('should get token', (done) => {
        common.loginWithDefaultUser().then(res => {
            done();
        });
    });


    it('should create a valid doctor', (done) => {
        chai.request(url)
            .post('/doctor?token=' + token)
            .send({
                nombre: "Dr. Fake",
                user: persistentUser._id,
                hospital: persistentHospital._id
            })
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.doctor).to.be.an('object');
                expect(res.body.doctor).to.have.deep.property('nombre', 'Dr. Fake');
                expect(res).to.have.status(201);
                doctor = res.body.doctor;
                done();
            });
    });

    it('should get the doctor', (done) => {
        chai.request(url)
            .get('/doctor/' + doctor._id)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.doctor).to.be.an('object');
                expect(res.body.doctor).to.have.deep.property('nombre', 'Dr. Fake');
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should not get the doctor', (done) => {
        chai.request(url)
            .get('/doctor/' + ' ' + '/')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res).to.have.status(500);
                done();
            });
    });

    it('should not get the doctor does not exist', (done) => {
        chai.request(url)
            .get('/doctor/10000')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.description).to.deep.equal('Error loading doctors.');
                expect(res).to.have.status(500);
                done();
            });
    });

    it('should get all doctors', (done) => {
        chai.request(url)
            .get('/doctor')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.total === res.body.doctors.length).to.be.true
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should update doctor properties (nombre)', (done) => {
        doctor.nombre = "Dr. Richard Fake";

        chai.request(url)
            .put('/doctor/' + doctor._id + '?token=' + token)
            .send({
                nombre: doctor.nombre,
                user: doctor.user,
                hospital: doctor.hospital
            })
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body).to.be.an('object');
                expect(res.body.doctor).to.have.deep.property('nombre', doctor.nombre);
                expect(res.body.doctor).to.have.deep.property('user', doctor.user);
                expect(res.body.doctor).to.have.deep.property('hospital', doctor.hospital);
                expect(res).to.have.status(201);
                done();
            });
    });


    it('should delete the doctor', (done) => {
        chai.request(url)
            .delete('/doctor/' + doctor._id + '?token=' + token)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.doctor).to.have.deep.property('_id', doctor._id);
                expect(res.body.doctor._id == doctor._id).to.be.true;
                expect(res).to.have.status(201);
                done();

            });
    });

    it('should not delete a doctor that not exist', (done) => {
        var doctor_id = 1234;
        chai.request(url)
            .delete('/doctor/' + doctor_id + '?token=' + token)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res).to.have.status(500);
                done();

            });
    });

    it('should not update doctor that not exist (nombre)', (done) => {
        var nombre = "Dr. Richard Fake";
        var doctor_id = 1234;
        chai.request(url)
            .put('/doctor/' + doctor_id + '?token=' + token)
            .send({
                nombre: nombre,
                user: nombre,
                hospital: nombre
            })
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body).to.be.an('object');
                expect(res).to.have.status(500);
                done();
            });
    });

});