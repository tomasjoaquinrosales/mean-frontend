const common = require("./common.test");

var bcrypt = require('bcrypt');

// Import the dependencies for testing
const expect = require('chai').expect;
const should = require('chai').should();
const chai = require('chai');
const chaiHttp = require('chai-http');
const request = require("supertest");

var User = require("../models/user");
var Hospital = require("../models/hospital");

const url = 'http://localhost:3000';

const app = require("../app");

chai.use(chaiHttp);

const saltRounds = 10;
const salt = bcrypt.genSaltSync(saltRounds);


describe('Hospital', () => {

    var token = null;
    var persistentUser = null;
    var hospital = null;

    //get token
    before(async() => {
        var response = await common.loginWithDefaultUser();
        token = response.body.token;
        //console.log(token);
        persistentUser = response.body.user;
    });

    // delete user and hospital default
    after(async() => {
        await common.cleanAllUsers();
        await common.cleanAllHospitals();
    });

    it('should get token', (done) => {
        common.loginWithDefaultUser().then(res => {
            done();
        });
    });


    it('should create a valid hospital', (done) => {
        chai.request(url)
            .post('/hospital?token=' + token)
            .send({
                nombre: "Hospital Robinson Jr. H",
                user: persistentUser._id
            })
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.hospital).to.be.an('object');
                expect(res.body.hospital).to.have.deep.property('nombre', 'Hospital Robinson Jr. H');
                expect(res).to.have.status(201);
                hospital = res.body.hospital;
                done();
            });
    });

    it('should get the hospital', (done) => {
        chai.request(url)
            .get('/hospital/' + hospital._id + '?token=' + token)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.hospital).to.be.an('object');
                expect(res.body.hospital).to.have.deep.property('nombre', "Hospital Robinson Jr. H");
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should not get the hospital', (done) => {
        chai.request(url)
            .get('/hospital/1000?token=' + token)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.description).to.deep.equal('Error getting hospital with id: 1000');
                expect(res).to.have.status(500);
                done();
            });
    });

    it('should get all hospital', (done) => {
        chai.request(url)
            .get('/hospital')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.total === res.body.hospital.length).to.be.true
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should update hospital properties (nombre)', (done) => {
        hospital.nombre = "Hospital Richard Robinson Jr. H";

        chai.request(url)
            .put('/hospital/' + hospital._id + '?token=' + token)
            .send({
                nombre: hospital.nombre,
                user: persistentUser._id
            })
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body).to.be.an('object');
                expect(res.body.hospital).to.have.deep.property('nombre', hospital.nombre);
                expect(res.body.hospital).to.have.deep.property('user', hospital.user);
                expect(res).to.have.status(201);
                done();
            });
    });


    it('should delete the hospital', (done) => {
        chai.request(url)
            .delete('/hospital/' + hospital._id + '?token=' + token)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.hospital).to.have.deep.property('_id', hospital._id);
                expect(res.body.hospital._id == hospital._id).to.be.true;
                expect(res).to.have.status(201);
                done();

            });
    });

    it('should not delete a hospital that not exist', (done) => {
        var doctor_id = 1234;
        chai.request(url)
            .delete('/hospital/' + doctor_id + '?token=' + token)
            .end(function(err, res) {
                if (err) return done(err);
                expect(res).to.have.status(500);
                done();

            });
    });

    it('should not update hospital that not exist (nombre)', (done) => {
        var nombre = "Dr. Richard Fake";
        var hospital_id = 1234;
        chai.request(url)
            .put('/hospital/' + hospital_id + '?token=' + token)
            .send({
                nombre: nombre,
                user: persistentUser._idx,
            })
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body).to.be.an('object');
                expect(res).to.have.status(500);
                done();
            });
    });

});