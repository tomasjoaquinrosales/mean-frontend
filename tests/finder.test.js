const common = require("./common.test");

const expect = require('chai').expect;
const should = require('chai').should();
const chai = require('chai');
const chaiHttp = require('chai-http');
const request = require("supertest");

var User = require("../models/user");
const url = 'http://localhost:3000';

const app = require("../app");

chai.use(chaiHttp);

var bcrypt = require('bcrypt');

describe('Finder', () => {

    var token = null;
    var persistentUser = null;
    var persistentHospital = null;
    var persistentDoctor = null;

    //get token
    before(async() => {
        var response = await common.loginWithDefaultUser();
        token = response.body.token;
        //console.log(token);S
        persistentUser = response.body.user;
        persistentHospital = await common.getDefaultHospital();
        persistentDoctor = await common.getDefaultDoctor();
    });

    // delete user default
    after(async() => {
        await common.cleanAllUsers();
        await common.cleanAllHospitals();
        await common.cleanAllDoctors();
    });

    it('should get token', (done) => {
        common.loginWithDefaultUser().then(res => {
            done();
        });
    });

    it('should get all hospital, doctor and user that comply the following filter => name: Dr.', (done) => {
        chai.request(url)
            .get('/find/all/dr')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.hospitals).to.be.an('array');
                expect(res.body.doctors).to.be.an('array');
                expect(res.body.doctors[0]).to.have.deep.property('nombre', persistentDoctor.nombre);
                expect(res.body.hospitals[0]).to.have.deep.property('nombre', persistentHospital.nombre);
                expect(res).to.have.status(200);
                done();
            });
    });

    //http://localhost:3000/find/collection/user/tes
    it('should get all user collection that comply the following filter => name: express', (done) => {
        chai.request(url)
            .get('/find/collection/user/express')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.users).to.be.an('array');
                expect(res.body.users).to.be.an('array');
                expect(res.body.users[0]).to.have.deep.property('nombre', persistentUser.nombre);
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should get all hospital collection that comply the following filter => name: hospital', (done) => {
        chai.request(url)
            .get('/find/collection/hospital/hospital')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.hospitals).to.be.an('array');
                expect(res.body.hospitals).to.be.an('array');
                expect(res.body.hospitals[0]).to.have.deep.property('nombre', persistentHospital.nombre);
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should get all doctor collection that comply the following filter => name: foreman', (done) => {
        chai.request(url)
            .get('/find/collection/doctor/foreman')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.doctors).to.be.an('array');
                expect(res.body.doctors).to.be.an('array');
                expect(res.body.doctors[0]).to.have.deep.property('nombre', persistentDoctor.nombre);
                expect(res).to.have.status(200);
                done();
            });
    });

});