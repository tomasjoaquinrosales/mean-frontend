const common = require("./common.test");
const fs = require('fs');
const expect = require('chai').expect;
const should = require('chai').should();
const chai = require('chai');
const chaiHttp = require('chai-http');
const request = require("supertest");

var User = require("../models/user");
const url = 'http://localhost:3000';

const app = require("../app");

chai.use(chaiHttp);


describe('Image', () => {

    var token = null;
    var persistentUser = null;
    var persistentHospital = null;
    var persistentDoctor = null;

    //get token
    before(async() => {
        var response = await common.loginWithDefaultUser();
        token = response.body.token;
        //console.log(token);S
        persistentUser = response.body.user;
        persistentHospital = await common.getDefaultHospital();
        persistentDoctor = await common.getDefaultDoctor();
    });

    // delete user default
    after(async() => {
        await common.cleanAllUsers();
        await common.cleanAllHospitals();
        await common.cleanAllDoctors();
    });

    it('should get token', (done) => {
        common.loginWithDefaultUser().then(res => {
            done();
        });
    });

    it('should upload user image', (done) => {
        chai.request(url)
            .put('/upload/users/' + persistentUser._id)
            .field('Content-Type', 'multipart/form-data')
            .field('image', 'avatar.jpg')
            .attach('image', common.upload_local + 'avatar.jpg')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.user._id).to.deep.equal(persistentUser._id);
                expect(res.body.user).to.have.property('image');
                expect(res).to.have.status(200);
                persistentUser.image = res.body.user.image;
                done();
            });
    });

    it('should get user image', (done) => {
        chai.request(url)
            .get('/image/users/avatar.jpg')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should upload doctor image', (done) => {
        chai.request(url)
            .put('/upload/doctors/' + persistentDoctor._id)
            .field('Content-Type', 'multipart/form-data')
            .field('image', 'avatar.jpg')
            .attach('image', common.upload_local + 'avatar.jpg')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.doctor).to.have.property('image');
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should upload hospsital image', (done) => {
        chai.request(url)
            .put('/upload/hospitals/' + persistentHospital._id)
            .field('Content-Type', 'multipart/form-data')
            .field('image', 'avatar.jpg')
            .attach('image', common.upload_local + 'avatar.jpg')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body.hospital).to.have.property('image');
                expect(res).to.have.status(200);
                done();
            });
    });

    it('should not upload {$colletion}', (done) => {
        chai.request(url)
            .put('/upload/badly/' + persistentHospital._id)
            .field('Content-Type', 'multipart/form-data')
            .field('image', 'avatar.jpg')
            .attach('image', common.upload_local + 'avatar.jpg')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res).to.have.status(400);
                done();
            });
    });

    it('should not upload if user does not exist', (done) => {
        chai.request(url)
            .put('/upload/users/1234')
            .field('Content-Type', 'multipart/form-data')
            .field('image', 'avatar.jpg')
            .attach('image', common.upload_local + 'avatar.jpg')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res).to.have.status(400);
                done();
            });
    });

    it('should not upload if doctor does not exist', (done) => {
        chai.request(url)
            .put('/upload/doctors/1234')
            .field('Content-Type', 'multipart/form-data')
            .field('image', 'avatar.jpg')
            .attach('image', common.upload_local + 'avatar.jpg')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res).to.have.status(400);
                done();
            });
    });

    it('should not upload if hospital does not exist', (done) => {
        chai.request(url)
            .put('/upload/hospitals/1234')
            .field('Content-Type', 'multipart/form-data')
            .field('image', 'avatar.jpg')
            .attach('image', common.upload_local + 'avatar.jpg')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res).to.have.status(400);
                done();
            });
    });


    it('should not upload if iamges does not exist', (done) => {
        chai.request(url)
            .put('/upload/users/' + persistentUser._id)
            .field('Content-Type', 'multipart/form-data')
            .field('image', 'avatar.jpg')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body).to.be.deep.equal({ status: false, error: { message: 'No files were uploaded.' } });
                expect(res).to.have.status(400);
                done();
            });
    });

    it('should not upload user image by invalid extention', (done) => {
        chai.request(url)
            .put('/upload/users/' + persistentUser._id)
            .field('Content-Type', 'multipart/form-data')
            .field('image', 'download.tiff')
            .attach('image', common.upload_local + 'download.tiff')
            .end(function(err, res) {
                if (err) return done(err);
                expect(res.body).to.be.deep.equal({ status: false, error: { message: 'Extention Not Allowed.' } });
                expect(res).to.have.status(400);
                done();
            });
    });

});