const common = require("./common.test");

const expect = require('chai').expect;
const should = require('chai').should();
const chai = require('chai');
const chaiHttp = require('chai-http');
const request = require("supertest");

var User = require("../models/user");
const url = 'http://localhost:3000';

const app = require("../app");

chai.use(chaiHttp);


describe('Login', () => {

    var token = null;
    var persistentUser = null;
    var persistentHospital = null;
    var persistentDoctor = null;

    //get token
    before(async() => {
        var response = await common.loginWithDefaultUser();
        token = response.body.token;
        //console.log(token);
        persistentUser = response.body.user;
    });

    // delete user default
    after(async() => {
        await common.cleanAllUsers();
    });

    it('should get token', (done) => {
        common.loginWithDefaultUser().then(res => {
            done();
        });
    });


    it('should login with google');

});